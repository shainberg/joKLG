# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table products (
  product_id                numeric(19) identity(1,1) not null,
  product_name              varchar(255),
  kategory                  numeric(19),
  cost                      float(32),
  unit_id                   numeric(19),
  constraint pk_products primary key (product_id))
;




# --- !Downs

drop table products;

