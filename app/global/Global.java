package global;

import play.GlobalSettings;
import play.api.mvc.Handler;
import play.cache.Cache;
import play.mvc.Controller;
import play.mvc.Http.Cookie;
import play.mvc.Http.RequestHeader;

public class Global extends GlobalSettings{
	@Override
	public Handler onRouteRequest(RequestHeader arg0) {
		if (arg0.uri().startsWith("/assests") || arg0.uri().startsWith("/login")){
			return super.onRouteRequest(arg0);
		}
		
		Cookie cookie = arg0.cookie("token");
		if (cookie != null){
			String token = cookie.value();
			Integer userId = (Integer) Cache.get(token);
			
			if (userId != null){
				return super.onRouteRequest(arg0);
			}
		}
		
		return controllers.Default.redirect("/login");
	}
}
