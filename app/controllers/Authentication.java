package controllers;

import java.util.List;
import java.util.Random;

import models.Soilders;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;

import play.cache.Cache;
import play.mvc.Controller;
import play.mvc.Result;

public class Authentication extends Controller{
	public static Result login(){
		JsonNode requestparams = request().body().asJson();
		
		String name = requestparams.get("name").asText();
		String pass = requestparams.get("pass").asText();
		
		List<Soilders> soldier = Ebean.find(Soilders.class).where().eq("first_name", name).eq("password", pass).findList();
		
		if (soldier == null || soldier.size() != 1){
			return unauthorized();
		}
		
		long nextLong = new Random().nextLong();
		response().setCookie("token", String.valueOf(nextLong));
		Cache.set(name, nextLong);
		
		return redirect("/catalog");
	}
}
