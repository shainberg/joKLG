package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Soilders extends Model {
	private static final long serialVersionUID = 1L;

	@Id

    public Long personal_number;
	
	@Constraints.Required
	public String first_name;
	
	@Constraints.Required
	public String last_name;
	
	@Constraints.Required
	public Long position;
	
	@Constraints.Required
	public Long unit_id;

  
  
  public static Finder<Long,Soilders> find = new Finder<Long,Soilders>(
    Long.class, Soilders.class
  ); 
}