package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class unitsProducts extends Model {
	private static final long serialVersionUID = 1L;

  @Id
	public Long id; 
  
	@Constraints.Required
  public Long unit_id;
  
	@Constraints.Required
  public Long product_id;
	
	@Constraints.Required
	public Long amount;

  
  public static Finder<Long,unitsProducts> find = new Finder<Long,unitsProducts>(
    Long.class, unitsProducts.class
  ); 
}