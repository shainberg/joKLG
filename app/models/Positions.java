package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Positions extends Model {	
	private static final long serialVersionUID = 1L;

	@Id
  public Long position_id;
  
	  @Constraints.Required
  public String position_name;

	  @Constraints.Required
  public Long kategory_id;
  
  
  public static Finder<Long,Positions> find = new Finder<Long,Positions>(
    Long.class, Positions.class
  ); 
}