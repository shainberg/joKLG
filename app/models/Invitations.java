package models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.format.*;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Invitations extends Model {
	private static final long serialVersionUID = 1L;

	@Id
  public Long invitation_id;
	
	@Constraints.Required
  public Long inventor_id;
	
	@Constraints.Required
  public String status;
  
  @Constraints.Required
  public int unit_id;
  
  @Constraints.Required
  @Formats.DateTime(pattern="dd/MM/yyyy")
  public Date invitation_date;
  
   @Formats.DateTime(pattern="dd/MM/yyyy")
  public Date end_date;
   
   @Formats.DateTime(pattern="dd/MM/yyyy")
  public Date last_change_date;
  

  
  public static Finder<Long,Invitations> find = new Finder<Long,Invitations>(
    Long.class, Invitations.class
  ); 
}