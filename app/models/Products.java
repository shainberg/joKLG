package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Products extends Model {
	private static final long serialVersionUID = 1L;

	@Id

  public Long product_id;
  
  @Constraints.Required
  public String product_name;

  @Constraints.Required
  public Long kategory;
  
  @Constraints.Required
  public Double cost;
  
  public Long unit_id;
  
  public static Finder<Long,Products> find = new Finder<Long,Products>(
    Long.class, Products.class
  ); 

}