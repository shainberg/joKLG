package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Units extends Model {
	private static final long serialVersionUID = 1L;

	@Id
  public Long unit_id;
  
	@Constraints.Required
  public String unit_name;

  
  public static Finder<Long,Units> find = new Finder<Long,Units>(
    Long.class, Units.class
  ); 
}
