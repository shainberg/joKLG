package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class Kategories extends Model {
	private static final long serialVersionUID = 1L;

	@Id
  public Long kategory_id;
	
	@Constraints.Required
  public String kategory_name;
  

  
  public static Finder<Long,Kategories> find = new Finder<Long,Kategories>(
    Long.class, Kategories.class
  ); 
}