package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity 
public class ProductsInvitations extends Model {
	private static final long serialVersionUID = 1L;

	@Id
  public Long product_id;
  
	@Id
  public Long invitation_id;
	
	@Constraints.Required
  public Long amount;
  
  
  
  
  public static Finder<Long,ProductsInvitations> find = new Finder<Long,ProductsInvitations>(
    Long.class, ProductsInvitations.class
  ); 
}